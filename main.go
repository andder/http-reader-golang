package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

type logWriter struct{}

func main() {
	resp, err := http.Get("http://www.google.com")
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	lw := logWriter{}

	io.Copy(lw, resp.Body)
}

func (logWriter) Write(bSlice []byte) (int, error) {
	fmt.Println(string(bSlice))
	fmt.Println("Length of Byte Slice:", len(bSlice))
	return len(bSlice), nil
}
